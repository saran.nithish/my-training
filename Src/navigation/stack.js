import React from 'react';
// Library
import { createStackNavigator } from '@react-navigation/stack';
import TrainingScreen from '../Screen/TrainingScreen';
import Tabs from '../Screen/Tabs';
import LegsToning from '../Screen/LegsToning';
import WorkScreen from '../Screen/WorkScreen';
import YourAmisScreen from '../Screen/YourAmisScreen';





const Stack = createStackNavigator();

const StackScreens = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="work" headerMode="screen">

      <Stack.Screen
        name="Tabs"
        component={Tabs}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name="Training"
        component={TrainingScreen}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name="Leg"
        component={LegsToning}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="work"
        component={WorkScreen}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen
        name="youramis"
        component={YourAmisScreen}
        options={{
          headerShown: false
        }}
      />


    </Stack.Navigator>
  );
};

export default StackScreens;
