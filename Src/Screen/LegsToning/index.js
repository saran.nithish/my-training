import React,{useState} from 'react';
import { View, Text, Image, StatusBar, ScrollView, FlatList, TouchableOpacity,Model } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globalstyles/styles';
import globalColor from '../../globalstyles/color';
import styles from './styles';
/////library
import Icon from 'react-native-vector-icons/Ionicons';
import Material from 'react-native-vector-icons/MaterialIcons';
import Mat from 'react-native-vector-icons/MaterialCommunityIcons';
import { imagepath } from '../../globalstyles/ImagePath'
import LinearGradient from 'react-native-linear-gradient';
import Font5 from 'react-native-vector-icons/FontAwesome5'
import LegList  from '../../Components/LegsList';

const LegsToning = (props) => {
   
   

    return (
        <LinearGradient colors={[globalColor.cri, globalColor.cri2]}>
            <View style={globlaStyle.global}>
                <StatusBar backgroundColor={'transparent'} translucent={true} />
                <View
                    style={styles.chartbox} >
                    <View style={styles.chatboxinner}>
                        <View style={globlaStyle.row}>
                            <View style={globlaStyle.flex}>
                                <TouchableOpacity onPress={() => { props.navigation.navigate('Training') }}>
                                    <Icon
                                        name="arrow-back-outline"
                                        size={hp('3%')}
                                        color={globalColor.white}

                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={globlaStyle.flex}>
                                <View style={styles.idea}>
                                    <Icon
                                        name="information-circle-outline"
                                        size={hp('3%')}
                                        color={globalColor.white}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styles.legsView}>
                            <Text style={styles.legs}> Legs Toning </Text>
                        </View>
                        <View style={styles.workout}>
                            <Text style={styles.legs}> and Glutes Workout</Text>
                        </View>
                        <View>
                            <View style={[globlaStyle.row, styles.topspace]}>
                                <View style={globlaStyle.flex}>
                                    <View style={styles.timer}>
                                        <View style={globlaStyle.row}>
                                            <Material
                                                name="timer"
                                                size={hp('2%')}
                                                color={globalColor.white}
                                            />
                                            <View style={styles.minView}>
                                                <Text style={styles.min}>68 min</Text>
                                            </View>
                                        </View>
                                    </View>

                                </View>
                                <View style={[globlaStyle.flex, styles.maginR]}>
                                    <View style={styles.dumb}>
                                        <View style={globlaStyle.row}>
                                            <Mat
                                                name="dumbbell"
                                                size={hp('2%')}
                                                color={globalColor.white}
                                            />
                                            <View style={styles.minView}>
                                                <Text style={styles.min}>Resistant band,kettlebell</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.cont2}>
                    <View style={styles.subcont}>

                        <View style={globlaStyle.row}>
                            <View style={globlaStyle.flex}>
                                <Text style={styles.legtitle}>Cricuit 1: Legs Toning</Text>
                            </View>
                            <View style={[globlaStyle.flex, styles.setsView]}>
                                <View style={globlaStyle.row}>
                                    <Material
                                        name="wifi-protected-setup"
                                        size={hp('2.5%')}
                                        color={globalColor.lightgray}
                                    />
                                    <Text style={styles.setsText}>3 sets</Text>
                                </View>
                            </View>
                        </View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <LegList/>
                        </ScrollView>
                        <View style={styles.btnView}>
                            <LinearGradient
                                colors={[globalColor.bnt1, globalColor.bnt2]}
                                style={styles.btnGradient}>
                                <View style={[globlaStyle.row, styles.justifyContent]}>
                                    <Font5
                                        name="burn"
                                        size={hp('3%')}
                                        color={globalColor.white}
                                        style={styles.burnIcon}
                                    />
                                    <View style={styles.startTextview}>
                                        <Text style={styles.btnTxt}>Start workout</Text>
                                    </View>
                                </View>
                            </LinearGradient>
                        </View>
                    </View>
                </View>
            </View>
        </LinearGradient>
    )
};



export default LegsToning;