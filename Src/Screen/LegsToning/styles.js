import { StyleSheet } from 'react-native';
///font & color
import globalColor from '../../globalstyles/color';
import { fontProperties } from '../../globalstyles/font';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({

    chartbox: {
        flex: 0.4
    },
    chatboxinner: {
        padding: hp('2%'),
        paddingVertical: hp('5%'),
        paddingHorizontal:hp('3%'),
    },
    idea: {
        alignItems: 'flex-end',
        right: hp('2%')
    },
    legs: {
        fontSize: hp('2.3%'),
        fontFamily: fontProperties.Medium,
        color: globalColor.white
    },
    legsView: {
        paddingTop: hp('2%'),
        //marginLeft: hp('1%')
    },
    workout: {
//marginLeft: hp('1%')
    },
    minView: {
        marginLeft: hp('1%'),
        top: 1
    },
    min: {
        fontSize: hp('1.5%'),
        fontFamily: fontProperties.regular,
        color: globalColor.white
    },
    dumb: {
        backgroundColor: globalColor.boxcolor,
        paddingVertical: hp('.8%'),
        width: hp('26%'),
        alignItems: 'center', borderRadius: hp('1%')
    },
    timer: {
        backgroundColor: globalColor.boxcolor,
        paddingVertical: hp('.8%'),
        width: hp('12%'),
        alignItems: 'center',
        borderRadius: hp('1%')
    },
    maginR: {
        marginRight: hp('15%')
    },
    topspace: {
        paddingVertical: hp('5%')
    },
    cont2: {
        backgroundColor: globalColor.white,
        flex: 1,
        borderTopEndRadius: hp('10%')
    },
    subcont: {
        margin: hp('3%')
    },
    setsView: {
        alignItems: 'flex-end',
        right: hp('4%')
    },
    legtitle: {
        fontSize: hp('2%'),
        fontFamily: fontProperties.semiBold,
        color: globalColor.black
    },
    setsText: {
        fontSize: hp('1.8%'),
        fontFamily: fontProperties.regular,
        color: globalColor.lightgray
    },
    btnTxt: {
        color:globalColor.white,
        fontSize: hp('2%'),
        fontFamily: fontProperties.semiBold,
        //textTransform: 'capitalize',
       
    },
    btnGradient: {
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: hp('1.8%'),
    },
    btnView: {
        position:'relative',
        paddingHorizontal: hp('4%'),
        bottom: hp('10%')
    },
    startTextview: {
        paddingTop: hp('1%'),
        marginLeft: hp('2%')
    },
    justifyContent: {
        justifyContent: 'center'
    },
    imgView: {
        height: hp('10%'),
        width: hp('15%'),
    },
    imgeFlex:{
        flex:0.6,
        alignItems:'flex-start',
        right:hp('1%')
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius:hp('2%')
    },
    itemtitle: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2%'),
        color: globalColor.titleitem
    },
    itemsec: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.8%'),
        color: globalColor.lightgray
    },
    flastlist:{
        paddingVertical:hp('1%')
    },
   Bottomlinespace:{
        paddingVertical:hp('2%'),
    },
    circleShap2: {
        elevation: 4,
        position: "absolute",
        zIndex: 100,
        right: hp('5%'),
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp('3%'),
        height: hp('4%'),
        width: hp('4%'),
        borderRadius: hp('4%'),
        backgroundColor: "#fff"
      },
    playIconStyle:{
        alignSelf: 'center',
        left:hp('.2%')
    },
    restsec:{
        backgroundColor: globalColor.secbox, 
        borderRadius: hp('1%'), 
        paddingHorizontal: hp('1%'), 
        top: hp('2.2%')
    },
    linespace:{
        top:hp('2%')
    },
    linespace2:{
       top:hp('3.8%')
    },
    sectext:{
        fontFamily:fontProperties.regular,
        fontSize:hp('1.5%'),
        color:globalColor.blueicon
    },
    linecolor:{
        color:globalColor.lightgray
    },
    burnIcon:{
        paddingTop:hp('.5%')
    }
});
