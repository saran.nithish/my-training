import { StyleSheet } from 'react-native';
///font & color
import globalColor from '../../globalstyles/color';
import { fontProperties } from '../../globalstyles/font';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({

    bg: {
        backgroundColor: globalColor.white
    },
    subCont: {
        paddingVertical: hp('6%'),
        paddingHorizontal: hp('2%')
    },
    heading: {
        fontWeight: 'bold',
        fontSize: hp('3%')
    },
    boxview: {
        paddingVertical: hp('2%'),
        paddingHorizontal: hp('2%')
    },
    boxStyle: {
        width: wp('40%'),
        height: hp('25%'),
        borderRadius: hp('5%')
    },
    imgview: {
        bottom: hp('5%'),
        width: '80%',
        height: '70%',
        marginLeft:hp('1%')
    },
    img: {
        width: '100%',
        height: '100%'
    },
    hrs:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        color:globalColor.white
        //marginLeft:hp('3%')
    },
    htext:{
        fontSize:hp('1.5%'),
        fontWeight:'bold',
        color:globalColor.white
    },
    title:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        color:globalColor.white
    },
    itemtextView:{
        bottom: hp('5%'), 
        marginLeft: hp('2%')
    },
    itemviewSpace:{
        paddingVertical:hp('1.5%')
    },
    listview:{
        paddingVertical: hp('4%')
    },
    notificationbox:{
        backgroundColor:'#F5FFFA',
        height:hp('15%'),
        borderTopRightRadius:hp('5%'),
        borderBottomLeftRadius:hp('5%'),
        borderBottomRightRadius:hp('5%'),
        padding:hp('2%'),
        bottom:hp('3%')
    },
    crossIconView:{
        backgroundColor:globalColor.white,
        height:hp('5%'),
        width:hp('5%'),
        borderRadius:hp('2%'),
        alignItems:'center',
        justifyContent:'center'
    },
    bell:{
        flex:0.2,
        justifyContent:'center'
    },
    learnText:{
        flex:1,
        justifyContent:'center'
    },
    cross:{
        flex:0.2,
        justifyContent:'center'
    },
    learnTextsty:{
        fontSize:hp('1.8%'),
        color:'gray'
    },
    rowView:{
        paddingTop:hp('2.5%')
    },
    seemoreview:{
        flex:1,
        alignItems:'flex-end',
        justifyContent:'center',
        paddingTop:hp('1%')
    },
    seemore:{
        fontSize:hp('2%'),
        color:'gray'
    },
    chartView:{
        paddingVertical:hp('2%'),
        right:hp('.5%')
    }
});
