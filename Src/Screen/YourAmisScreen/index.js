import React, { useState } from 'react';
import { View, Text, Image, StatusBar, ScrollView, FlatList, TouchableOpacity } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globalstyles/styles';
import globalColor from '../../globalstyles/color';
import styles from './styles';
/////library
import Entypo from 'react-native-vector-icons/Entypo';
import { imagepath } from '../../globalstyles/ImagePath'
import LinearGradient from 'react-native-linear-gradient';
import LineChart from '../../Components/lineChart';
import AmisList from '../../Components/AmisList';


const YourAmisScreen = (props) => {
   

    return (
        <View style={[globlaStyle.global, styles.bg]}>
            <View style={styles.subCont}>
                <View style={globlaStyle.row}>
                    <View>
                        <Text style={styles.heading}>Your Amis</Text>
                    </View>
                </View>
                <View style={styles.listview}>
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                    >
                        <AmisList/>
                    </ScrollView>
                </View>
                <View style={styles.notificationbox}>
                    <View style={[globlaStyle.row,styles.rowView]}>
                        <View style={styles.bell}>
                            <Entypo
                                name="bell"
                                size={hp('5%')}
                                color={globalColor.bellIcon}
                            />
                        </View>
                        <View style={styles.learnText}>
                            <Text style={styles.learnTextsty}>Learn more information  about
                         how to plan your daytime </Text>
                        </View>
                        <View style={styles.cross}>
                           <View style={styles.crossIconView}>
                           <Entypo
                                name="cross"
                                size={hp('3%')}
                                color={globalColor.crossIcon}
                            />
                           </View>
                        </View>
                    </View>
                </View>
                <View style={globlaStyle.row}>
                    <View style={globlaStyle.flex}>
                        <Text style={styles.heading}>OverView</Text>
                    </View>
                    <View style={styles.seemoreview}>
                        <Text style={styles.seemore}>See more</Text>
                    </View>
                </View>
                <View style={styles.chartView}>
                    <LineChart/>
                </View>
            </View>
        </View>
    )
};



export default YourAmisScreen;