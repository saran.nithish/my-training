import React from 'react';
import { View, Text, Image, StatusBar, ScrollView, FlatList, TouchableOpacity } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globalstyles/styles';
import globalColor from '../../globalstyles/color';
import styles from './styles';
/////library
import Icon from 'react-native-vector-icons/Ionicons';
import Font from 'react-native-vector-icons/FontAwesome';
import Font5 from 'react-native-vector-icons/FontAwesome5';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import Pie from 'react-native-pie';
import { imagepath } from '../../globalstyles/ImagePath';
import HomeFlatlist  from '../../Components/HomeFlatlist'


const HomeScreen = (props) => {
  const [skills, setskills] = React.useState([
    {
      id: '1',
      title: 'BreakFast',
      image: imagepath.food1,
      des: 'Bread,Peanut butter,Apple',
      bgcolor: globalColor.item1,
      bgcolor2: globalColor.item12,
      ka: '521'
    },
    {
      id: '2',
      title: 'Lunch',
      image: imagepath.food2,
      des: 'Salmon,Mixed veggles',
      bgcolor: globalColor.item2,
      bgcolor2: globalColor.item22,
      ka: '602'
    },

    {
      id: '3',
      title: 'Snack',
      image: imagepath.food2,
      des: 'Recommend:800 Kcal',
      //ka: '521',
      bgcolor: globalColor.item3,
      bgcolor2: globalColor.item32,
    },
  ])


  return (

    <View style={globlaStyle.global}>
      <StatusBar backgroundColor={'transparent'} translucent={true} />
      <View style={styles.subCont}>
        <ScrollView>
          <View style={globlaStyle.row}>
            <View style={globlaStyle.flex}>
              <View style={styles.tie}>
                <Text style={globlaStyle.headerTitle}>
                  {' My Diary'}
                </Text>
              </View>
            </View>
            <View style={[globlaStyle.flex, styles.end]}>
              <View style={[globlaStyle.row, styles.just]}>
                <View>
                  <Icon
                    name="chevron-back-outline"
                    size={hp('3%')}
                    color={globalColor.arrowicon}

                  />
                </View>
                <View style={[styles.forwordicon, styles.calView]}>
                  <Font
                    name="calendar-o"
                    size={hp('2%')}
                    color={globalColor.arrowicon}

                  />
                </View>
                <View style={styles.calView}>
                  <Text style={styles.caltext}>{'15 May'}</Text>
                </View>
                <View style={[styles.forwordicon, styles.forwordarrow]}>
                  <Icon
                    name="chevron-forward-outline"
                    size={hp('3%')}
                    color={globalColor.arrowicon}

                  />
                </View>
              </View>
            </View>
          </View>
          <View style={[globlaStyle.row, styles.jus]}>
            <View style={globlaStyle.flex}>
              <View style={styles.med}>
                <Text style={globlaStyle.subtitle}>{'Mediterranean diet'}</Text>
              </View>
            </View>
            <View style={[globlaStyle.flex, styles.detail]}>
              <View style={globlaStyle.row}>
                <View>
                  <Text style={styles.detailText}>{'Details'}  </Text>
                </View>
                <Icon
                  name="arrow-forward-outline"
                  size={hp('3%')}
                  color={globalColor.arrowicon}

                />
              </View>
            </View>
          </View>
          <View style={[globlaStyle.flex, styles.chartView, styles.viewspace]}>
            <View style={styles.chartbox}>
              <View style={styles.chatboxinner}>
                <View style={[globlaStyle.row, styles.chartboderline]}>
                  <View size={globlaStyle.flex}>
                    {/* <View style={globlaStyle.row}> */}
                    {/* //<View style={styles.RectangleShapeView} /> */}
                    <View>
                      <Text style={styles.boxtitle}>
                        Eaten
                         </Text>
                    </View>
                    <View style={globlaStyle.row}>
                      <Material
                        name="food-fork-drink"
                        size={hp('4.5%')}
                        color={globalColor.EastenColor}
                      />
                      <View style={styles.calnumview}>
                        <Text style={styles.calatext}>1128</Text>
                      </View>
                      <View style={styles.calTextView}>
                        <Text style={styles.calnum}>kcal </Text>
                      </View>
                    </View>
                    {/* </View> */}
                    <View>
                      <Text style={styles.boxtitle}>
                        Burned
                         </Text>
                    </View>
                    <View style={globlaStyle.row}>
                      <Font5
                        name="burn"
                        size={hp('4.5%')}
                        color={globalColor.burnedColor}

                      />
                      <View style={styles.calnumview}>
                        <Text style={styles.calatext}>102</Text>
                      </View>
                      <View style={styles.calTextView}>
                        <Text style={styles.calnum}>kcal </Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.cricleView}>
                    <View>
                      <Pie
                        radius={75}
                        innerRadius={61}
                        sections={[
                          {
                            percentage: 20,
                            color: globalColor.cri,
                          },
                          {
                            percentage: 20,
                            color: globalColor.cri2,
                          },
                        ]}

                        backgroundColor={globalColor.chart}
                      />
                      <View style={styles.gauge} >
                        <Text style={styles.gaugeText}>1502</Text>
                        <Text style={styles.calleft}>Kcal 1eft</Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={[globlaStyle.row, styles.viewspace]}>
                  <View>
                    <Text style={styles.CarbText}>Carbs</Text>
                    <View style={styles.RectangleShapeView3} />
                    <Text style={styles.calnum}>12g 1eft </Text>
                  </View>
                  <View style={styles.cardviem}>
                    <Text style={styles.CarbText}>Protein</Text>
                    <View style={styles.RectangleShapeView4} />
                    <Text style={styles.calnum}>12g 1eft </Text>
                  </View>
                  <View style={styles.cardviem}>
                    <Text style={styles.CarbText}>Fat</Text>
                    <View style={styles.RectangleShapeView5} />
                    <Text style={styles.calnum}>12g 1eft </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={[globlaStyle.row, styles.justs]}>
            <View style={globlaStyle.flex}>
              <View style={styles.med}>
                <Text style={globlaStyle.subtitle}>{'Meals today'}</Text>
              </View>
            </View>
            <View style={[globlaStyle.flex, styles.detail]}>
              <View style={globlaStyle.row}>
                <View>
                  <Text style={styles.detailText}>{'Customize'}  </Text>
                </View>
                <Icon
                  name="arrow-forward-outline"
                  size={hp('3%')}
                  color={globalColor.arrowicon}
                />
              </View>
            </View>
          </View>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            horizontal={true}
          >
           <HomeFlatlist navigation={props.navigation}/>
            
          </ScrollView>
          <View style={[globlaStyle.row, styles.just,styles.bottomview]}>
            <View style={globlaStyle.flex}>
              <View style={styles.med}>
                <Text style={globlaStyle.subtitle}>{'Body Measurement'}</Text>
              </View>
            </View>
            <View style={[globlaStyle.flex, styles.detail]}>
              <View style={globlaStyle.row}>
                <View style={styles.deView}>
                  <Text style={styles.detailText}>{'Today'}  </Text>
                </View>
                <Icon
                  name="arrow-forward-outline"
                  size={hp('3%')}
                  color={globalColor.arrowicon}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  )
};


export default HomeScreen;
