import { StyleSheet } from 'react-native';
///font & color
import globalColor from '../../globalstyles/color';
import { fontProperties } from '../../globalstyles/font';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
    subCont: {
        flex: 1,
        margin: hp('2%'),
        top:hp('3.2%')
    },
    forwordicon: {
        marginLeft: hp('1.5%')
    },
    just: {
        paddingTop: hp('.6%'),

    },
    justs: {
        paddingTop: hp('2%'),

    },
    jus: {
        paddingTop: hp('1.5%'),

    },
    caltext: {
        fontFamily: fontProperties.regular,
        color: globalColor.arrowicon,
        fontSize: hp('2%'),
        top: hp('.5%')
    },
    calView: {
        justifyContent: 'center',
        marginLeft: hp('1%')
        //top:hp('1%')
    },
    forwordarrow: {
        top: hp('.25%')
    },
    end: {
        alignItems: 'flex-end',
        right:hp('2%')
    },
    med: {
        marginLeft: hp('1%'),
    },
    chartView: {
        paddingHorizontal:hp('2%'),
        right:hp('1%')
    },
    viewspace: {
        paddingVertical: hp('1.5%')
    },
    detail: {
        alignItems: 'flex-end',
        right: hp('1%')
    },
    detailText: {
        fontSize: hp('1.8%'),
        fontFamily: fontProperties.regular,
        color: globalColor.blueicon
    },
    chartbox: {
        backgroundColor: globalColor.white,
        borderTopEndRadius: hp('10%'),
        borderBottomRightRadius: hp('2%'),
        borderBottomStartRadius: hp('2%'),
        borderTopStartRadius: hp('2%'),
        elevation: 3
    },
    chatboxinner: {
        padding: hp('2%')
    },
    RectangleShapeView: {
        marginTop: 20,
        width: hp('.5%'),
        height: hp('8%'),
        backgroundColor: globalColor.EastenColor
    },
    RectangleShapeView3: {
       // marginTop: 20,
        width: hp('6%'),
        height: hp('.5%'),
        backgroundColor: globalColor.EastenColor
    },
    RectangleShapeView4: {
        // marginTop: 20,
         width: hp('6%'),
         height: hp('.5%'),
         backgroundColor: globalColor.burnedColor
     },
     RectangleShapeView5: {
        // marginTop: 20,
         width: hp('6%'),
         height: hp('.5%'),
         backgroundColor: globalColor.fat
     },
    RectangleShapeView2: {
        marginTop: 20,
        width: hp('.5%'),
        height: hp('8%'),
        backgroundColor: globalColor.burnedColor
    },
    boxtitle: {
        fontFamily: fontProperties.regular,
        fontSize: hp('2%'),
        color: globalColor.lightgray

    },
    calatext: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('3.5%'),
        color: globalColor.black,
        letterSpacing:2
    },
    calnum: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.5%'),
        top:hp('.5%'),
        color: globalColor.lightgray
    },
    calleft: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.8%'),
        color: globalColor.lightgray,
        bottom:hp('1.2%')
    },
    calnumview: {
        marginLeft: hp('1%'),
        paddingTop: hp('.5%')
    },
    calTextView: {
        marginLeft: hp('1%'),
        paddingTop: hp('2.2%')
    },
    CarbText: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.9%'),
        color: globalColor.black
    },
    cardviem: {
        marginLeft: hp('8%')
    },
    cricleView: {
        flex: 1,
        alignItems: 'center',
        
    },
    gauge: {
        position: 'absolute',
        width: 100,
        height: 160,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: hp('3%')
    },
    gaugeText: {
        backgroundColor: 'transparent',
        color: globalColor.blueicon,
        fontFamily: fontProperties.bold,
        fontSize: hp('3%'),
        letterSpacing: 2

    },
    chartboderline: {
        borderBottomWidth: hp('.15%'),
        borderBottomColor: globalColor.bottomcolor,
        paddingBottom: hp('2%')
    },
   
   
    img: {
        width: '100%',
        height: '100%'
    },
   
    tie:{
        marginLeft:hp('.2%'),
        //paddingTop:hp('1%')
    },
    dumb: {
        backgroundColor: globalColor.boxcolor,
       // paddingVertical: hp('.8%'),
        width: hp('10%'),
        height:hp('10%'),
        alignItems: 'center', 
        borderBottomRightRadius:hp('10%')
    },
    bottomview:{
        marginLeft: hp('1%')
    }
});
