import { StyleSheet } from 'react-native';
///font & color
import globalColor from '../../globalstyles/color';
import { fontProperties } from '../../globalstyles/font';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
    img: {
        width: '100%',
        height: '100%'
    },
    profileimageview: {
        flex: 0.5
    },
    top: {
        paddingVertical: hp('5%'),
        paddingHorizontal: hp('2%')
    },
    top2: {
        paddingVertical: hp('2%'),
        marginLeft:hp('3%')
    },
    CircleShape: {
        width: hp('7%'),
        height: hp('7%'),
        borderRadius: hp('3%'),
        backgroundColor: '#7DDCFE',
        alignItems:'center'
    },
    cricleView:{
        marginLeft:hp('38%')
    },
    dotText:{
        textAlign:'center',
        color:globalColor.white,
        fontWeight:'bold',
        fontSize:hp('2.5%'),
        bottom:hp('1%')
    },
    CircleShape2: {
        width: hp('40%'),
        height: hp('40%'),
        borderRadius: hp('40%'),
        borderColor:globalColor.white,
        borderWidth:hp('.10%')
       //backgroundColor:globalColor.white,
    },
    CircleShape3: {
        width: hp('35%'),
        height: hp('35%'),
        borderRadius: hp('35%'),
        //borderColor:globalColor.white,
       // borderWidth:hp('.10%'),
       backgroundColor:globalColor.white,
    },
    circle2view:{
        alignItems: 'center', 
        //paddingVertical: hp('1%')
    },
    circleview3:{
        alignItems: 'center', 
        paddingTop: hp('2%') 
    },
    cont2:{
        backgroundColor:globalColor.white,
        height:'50%',width:'100%',
        borderTopRightRadius:hp('10%'),
        //bottom:hp('1%')
    },
    bg:{
        backgroundColor:'#fff'
    },
    cont1:{
        width:'100%',
        height:hp('70%'),
        borderBottomLeftRadius:hp('8%')
    },
    mealboxview: {
        paddingHorizontal: hp('2%'),
        paddingVertical: hp('2.5%')
    },
    imgview: {
        bottom: hp('4%'),
        width: '50%',
        height: '38%',
        marginLeft:hp('1%')
    },
    img2: {
        width: '50%',
        height: '50%',
        borderRadius:hp('2%')
       
    },
    activeText:{
        textAlign:'center',
        fontWeight:'bold',
        color:globalColor.white
    },
    itemimgeview:{
        height:hp('8%'),
        width:hp('8%'),
        alignItems:'center',
        justifyContent:'center'
    },
    textview:{
        paddingVertical:hp('1%')
    },
    actstyle:{
        //bottom:hp('1.2%')
    },
   
    lastText:{
        fontWeight:'bold',
        fontSize:hp('2%')
    },
    linercont:{
        width: '100%', 
        height: hp('100%'), 
        borderBottomLeftRadius: hp('10%')
    },
    worktext:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        left:hp('2%')
    },
    workView:{
        paddingVertical:hp('1%')
    },
    worktextview:{
        justifyContent:'center',
        flex:0.35,
    },
    workimage:{
        flex:0.3,
      
    },
    viewprogressbar:{
        alignItems:'center',
        paddingTop:hp('1.5%')
    },
    loaderView:{
        justifyContent:'center',
        flex:0.38,
        paddingTop:hp('.5%')
    },
    hrs:{
        fontSize:hp('3%'),
        fontWeight:'bold',
        marginLeft:hp('3%')
    },
    workhrsview:{
        flex:0.3,
        justifyContent:'center',
        //alignItems:'center'
        
    },
    htext:{
        fontSize:hp('1.8%'),
        fontWeight:'bold'
    },
    imgebg:{
        borderRadius:hp('3%')
    },
});
