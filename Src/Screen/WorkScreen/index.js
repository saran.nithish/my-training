import React, { useState } from 'react';
import { View, Text, Image, StatusBar, ScrollView,SafeAreaView, } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globalstyles/styles';
import globalColor from '../../globalstyles/color';
import styles from './styles';
/////library
import Entypo from 'react-native-vector-icons/Entypo';
import { imagepath } from '../../globalstyles/ImagePath'
import LinearGradient from 'react-native-linear-gradient';
import CricleProgressBar from '../../Components/CircularProgress';
import WorkList from '../../Components/Worklist';
import WorkSection from '../../Components/WorkSection';

const WorkScreen = (props) => {
   
    return (
        <SafeAreaView style={[globlaStyle.global, styles.bg]}>
            <LinearGradient colors={[globalColor.bgcolors, globalColor.bgcolors2]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}
                style={styles.cont1} >
                <StatusBar backgroundColor={'transparent'} translucent={true} />
                <View style={styles.top}>
                    <View styles={globlaStyle.row}>
                        <View style={globlaStyle.flex}>
                            <Image source={imagepath.profile}
                                style={globlaStyle.profileimage} />
                        </View>
                        <View style={styles.cricleView}>
                            <View style={styles.CircleShape}>
                                <View>
                                    <Entypo
                                        name="dot-single"
                                        size={hp('3%')}
                                        color={globalColor.white}
                                    />
                                </View>
                                <View style={globlaStyle.row}>
                                <Entypo
                                        name="dot-single"
                                        size={hp('3%')}
                                        color={globalColor.white}
                                    />
                                     <Entypo
                                        name="dot-single"
                                        size={hp('3%')}
                                        color={globalColor.white}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.circle2view}>
                        <View style={styles.CircleShape2}>
                            <View style={styles.circleview3}>
                                <View style={styles.CircleShape3}>
                                    <View style={styles.viewprogressbar}>
                                        <CricleProgressBar />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                    >
                        <WorkSection navigation={props.navigation}/>
                    </ScrollView>
                </View>
            </LinearGradient>
            <LinearGradient colors={[globalColor.bgcolors, globalColor.bgcolors2]}
                start={{ x: 0, y: 1 }}
                end={{ x: 0, y: 0 }}
                style={styles.linercont} >
                <View style={styles.cont2}>
                    <View style={styles.top2}>
                        <Text style={styles.lastText}>Last Activity</Text>
                        <WorkList/>
                    </View>
                </View>
            </LinearGradient>
        </SafeAreaView>
    )
};



export default WorkScreen;