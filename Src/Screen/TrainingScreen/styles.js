import { StyleSheet } from 'react-native';
///font & color
import globalColor from '../../globalstyles/color';
import { fontProperties } from '../../globalstyles/font';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
    subCont: {
        flex: 1,
        margin: hp('1%'),
        top:hp('3.2%')
        
    },
    forwordicon: {
        marginLeft: hp('1.5%')
    },
    just: {
        paddingTop: hp('2%'),

    },
    caltext: {
        fontFamily: fontProperties.regular,
        color: globalColor.arrowicon,
        fontSize: hp('1.8%'),
        top: hp('.4%')
    },
    calView: {
        justifyContent: 'center',
        marginLeft: hp('1%')
        //top:hp('1%')
    },
    forwordarrow: {
       top:hp('.3%')
    },
    end: {
        alignItems: 'flex-end',
        right:hp('2%')
    },
    med: {
        marginLeft: hp('1.3%')
    },
    chartView: {
        margin: hp('1%')
    },
    viewspace: {
        //paddingVertical: hp('10%')
    },
    detail: {
        alignItems: 'flex-end',
        right: hp('1%')
    },
    detailText: {
        fontSize: hp('1.8%'),
        fontFamily: fontProperties.regular,
        color: globalColor.blueicon
    },
    chartbox: {
        //backgroundColor: globalColor.white,
        height: hp('30%'),
        width: '95%',
        //borderRadius:hp('10%')
        borderTopLeftRadius: hp('2%'),
        borderTopRightRadius: hp('15%'),
        borderBottomRightRadius: hp('2%'),
        borderBottomLeftRadius: hp('2%'),
        elevation: 3
    },
    chatboxinner: {
        padding: hp('2%')
    },
    timerspace: {
        paddingTop: hp('8%'),
        paddingHorizontal: hp('3%')
    },
    minView: {
        marginLeft: hp('1%'),
        top:hp('.5%')
    },
    img: {
        width: '100%',
        height: '100%'
    },
    addimage: {
        width: '100%', 
        height: '50%',
        bottom:hp('3%'),
        marginLeft:hp('6%')
    },
    adv: {
        backgroundColor: globalColor.white,
        borderTopEndRadius: hp('2%'),
        borderBottomRightRadius: hp('2%'),
        borderBottomStartRadius: hp('2%'),
        borderTopStartRadius: hp('2%'),
        elevation: 3,
    },
    advView: {
        margin:hp('1.5%'),
    },
    area: {
        marginLeft: hp('2%'),
        paddingVertical:hp('1.5%')
    },
    card: {
        width: '50%',
        height:'60%',
       paddingVertical: hp('1.5%'),
        paddingHorizontal: hp('1.5%'),
      },
      subcard: {
        backgroundColor:globalColor.white,
        borderRadius: hp('1.5%'),
        paddingVertical:hp('2%'),
        elevation: 3,
        
      },
   itemimg:{
     width: hp('12%'), 
    height: hp('12%'), 
    borderRadius: hp('12%'), 
    alignSelf: 'center'

    },
    itemtext:{
      textAlign:'center',
      //bottom:4,
      top:hp('.5%'),
      fontFamily:fontProperties.regular,
      fontSize:hp('1.8%'),
      color:globalColor.blueicon

    },
    areaText:{
        fontSize:hp('2%'),
        fontFamily:fontProperties.semiBold,
        color:globalColor.black
    },
    flastlist:{
       // paddingVertical:hp('2%'),
       bottom:hp('1%')
       
    },
    doing:{
        fontFamily:fontProperties.semiBold,
        fontSize:hp('2%'),
        color:globalColor.blueicon
    },
    keep:{
        fontFamily:fontProperties.regular,
        fontSize:hp('1.5%'),
        color:globalColor.lightgray,
        left:hp('.4%'),
       
    },
    keepview:{
        bottom:hp('.5%')
    },
    doingView:{
        marginRight:hp('2%'),
        paddingVertical:hp('1%')
    },
    workout:{
       fontSize:hp('1.8%'),
       fontFamily:fontProperties.regular,
       color:globalColor.white
    },
    legs:{
        fontSize:hp('2.3%'),
        fontFamily:fontProperties.Medium,
        color:globalColor.white
    },
    legsView:{
        paddingTop:hp('1%')
    },
    min:{
        fontSize:hp('1.5%'),
        fontFamily:fontProperties.regular,
        color:globalColor.white
    },
    run:{
        height:90,
        width:60,
        bottom:hp('2%'),
        marginLeft:hp('2%')
    },
    runview:{
        flex:0.6
    },
    tie:{
        marginLeft:hp('1%'),
        paddingTop:hp('1%')
    },
    imagespace:{
        paddingVertical:hp('2%')
    }
});
