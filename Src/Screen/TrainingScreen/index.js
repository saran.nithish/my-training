import React,{useEffect} from 'react';
import { View, Text, Image, StatusBar, ScrollView,  TouchableOpacity,LogBox,SafeAreaView } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../../globalstyles/styles';
import globalColor from '../../globalstyles/color';
import styles from './styles';
/////library
import Icon from 'react-native-vector-icons/Ionicons';
import Material from 'react-native-vector-icons/MaterialIcons';
import Font from 'react-native-vector-icons/FontAwesome';
import { imagepath } from '../../globalstyles/ImagePath'
import LinearGradient from 'react-native-linear-gradient';
import TrainingList from '../../Components/TrainingList';

const TrainingScreen = (props) => {
    useEffect(() => {
        LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    }, [])

    return (

        <View style={globlaStyle.global}>
            <StatusBar style={globalColor.backgoundcolors} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.subCont}>
                    <View style={globlaStyle.row}>
                        <View style={globlaStyle.flex}>
                            <View style={styles.tie}>
                                <Text style={globlaStyle.headerTitle}>
                                    {'Training'}
                                </Text>
                            </View>
                        </View>
                        <View style={[globlaStyle.flex, styles.end]}>
                            <View style={[globlaStyle.row, styles.just]}>
                                <View>
                                    <Icon
                                        name="chevron-back-outline"
                                        size={hp('3%')}
                                        color={globalColor.arrowicon}
                                       // style={{bottom:hp('.2%')}}

                                    />
                                </View>
                                <View style={[styles.forwordicon, styles.calView]}>
                                    <Font
                                        name="calendar-o"
                                        size={hp('1.8%')}
                                        color={globalColor.arrowicon}
                                       

                                    />
                                </View>
                                <View style={styles.calView}>
                                    <Text style={styles.caltext}>{'29 May'}</Text>
                                </View>
                                <View style={[styles.forwordicon, styles.forwordarrow]}>
                                    <Icon
                                        name="chevron-forward-outline"
                                        size={hp('3%')}
                                        color={globalColor.arrowicon}

                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={[globlaStyle.row, styles.just]}>
                        <View style={globlaStyle.flex}>
                            <View style={styles.med}>
                                <Text style={globlaStyle.subtitle}>{'Your program'}</Text>
                            </View>
                        </View>
                        <View style={[globlaStyle.flex, styles.detail]}>
                            <View style={globlaStyle.row}>
                                <View>
                                    <Text style={styles.detailText}>{'Details'}  </Text>
                                </View>
                                <Icon
                                    name="arrow-forward-outline"
                                    size={hp('3%')}
                                    color={globalColor.arrowicon}

                                />
                            </View>
                        </View>
                    </View>
                    <View >
                        <LinearGradient colors={[globalColor.cri, globalColor.cri2]}
                            style={[styles.chartView, styles.viewspace, styles.chartbox]} >
                            <View style={styles.chatboxinner}>
                                <View>
                                    <Text style={styles.workout}>
                                        Next workout
                                </Text>
                                </View>
                                <View style={styles.legsView}>
                                    <Text style={styles.legs}>
                                        Legs Toning
                                </Text>
                                </View>
                                <View>
                                    <Text style={styles.legs}>
                                        and Glutes Workout
                                </Text>
                                </View>
                            </View>
                            <View style={[globlaStyle.row, styles.timerspace]}>
                                <View style={globlaStyle.flex}>
                                    <View style={globlaStyle.row}>
                                        <Material
                                            name="timer"
                                            size={hp('2.5%')}
                                            color={globalColor.white}
                                        />
                                        <View style={styles.minView}>
                                            <Text style={styles.min}>68 min</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={globlaStyle.flex}>
                                    <View style={styles.addimage}>
                                        <TouchableOpacity  onPress={() => { props.navigation.navigate('Leg') }}>
                                            <Image
                                                source={imagepath.playBtn}
                                                style={styles.img}
                                                resizeMode='contain'
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                    <View style={styles.advView}>
                        <View style={styles.adv}>
                            <View style={globlaStyle.row}>
                                <View style={styles.runview}>
                                    <View style={styles.run}>
                                        <Image
                                            source={imagepath.runimage}
                                            style={styles.img}
                                            resizeMode='stretch'
                                        />
                                    </View>
                                </View>
                                <View style={globlaStyle.flex}>
                                    <View style={styles.doingView}>
                                        <Text style={styles.doing}>
                                            Your are doing great
                                </Text>
                                <View style={styles.keepview}>
                                        <Text style={styles.keep}>
                                            Keet it up
                                </Text>
                                        <Text style={styles.keep}>
                                            and strick to your plan!
                                </Text>
                                </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.area}>
                        <Text style={styles.areaText}>Area of focus</Text>
                    </View>
                    
                    <TrainingList/>
                    
                
            </View>
            </ScrollView>
        </View>
    )
};



export default TrainingScreen;