import React from 'react';
////library
import Icon from 'react-native-vector-icons/Ionicons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View,  Image, TouchableOpacity,StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import imagepath from '../globalstyles/ImagePath'
import HomeScreen from './HomeScreen'
import globalColor from '../globalstyles/color'



const Tabs = () => {
    const Tab = createBottomTabNavigator();

    const TabbarCustomButton = ({ children, onPress }) => {
        return (
            <TouchableOpacity
                style={styles.touch} onPress={onPress}>
                <View
                    style={styles.button}>
                    {children}
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <Tab.Navigator
            swipeEnabled={true} tabBarOptions={{
                tabStyle: {
                    backgroundColor: '#fff',
                    borderTopRightRadius: hp('5%'),
                    borderTopEndRadius: hp('5%'),
                },
                backgroundColor: '#fff',
                activeTintColor: globalColor.activecolor,
                inactiveTintColor:globalColor.inactivecolor,
                inactiveColor: '#000',
                showLabel: false,
            }}
        >
         
            <Tab.Screen name="Home" component={HomeScreen}
                options={{
                    tabBarIcon: ({ color }) => (
                        <Icon name="newspaper-outline" color={color} size={25} />
                    ),
                }}
            />
            <Tab.Screen name="Menu" component={HomeScreen} options={{
                tabBarIcon: ({ color }) => (
                    <Icon name="grid-outline" color={color} size={25} />
                ),
            }} />
            <Tab.Screen name="Cart" component={HomeScreen} options={{
                tabBarIcon: ({ focused }) => (
                    <Image
                        source={imagepath.addBtn}
                        resizeMode={'contain'}
                        style={styles.cutombutton}
                    />
                ),
                tabBarButton: (props) => (
                    <TabbarCustomButton
                        {...props}
                    />
                )
            }} />
            <Tab.Screen name="Carts" component={HomeScreen} options={{
                tabBarIcon: ({ color }) => (
                    <Material name="food-apple-outline" color={color} size={30} />
                ),
            }} />
            <Tab.Screen name="Cartsa" component={HomeScreen} options={{
                tabBarIcon: ({ color }) => (
                    <AntDesign name="user" color={color} size={27} />
                ),
            }} />
        </Tab.Navigator>

    )
};


const styles = StyleSheet.create({
   touch:{
    top: -30,
    justifyContent: 'center',
    alignItems: 'center'
   },
   button:{
    width: hp('10%'),
    height: hp('5%'),
    borderRadius: hp('5%')
   },
   cutombutton:{
    width: hp('10%'),
    height: hp('10%'),
    borderRadius: hp('20%')
   }
  
  
  });
  



export default Tabs;
