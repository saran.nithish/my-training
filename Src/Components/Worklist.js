import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, FlatList,} from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../globalstyles/styles';
import globalColor from '../globalstyles/color';
/////library
import { imagepath } from '../globalstyles/ImagePath';
import LineLoader from '../Components/lineLoader';


const LegList = (props) => {
    const [work, setwork] = useState([
        {
            id: '1',
            title: 'Working',
            image: imagepath.work,

            bgcolor: globalColor.activeworkcolor,
            workhrs:'8',
            percentage:50
        },
        {
            id: '2',
            title: 'Sport',
            image: imagepath.football,
            des: 'Salmon,Mixed veggles',
            bgcolor: globalColor.activeworkcolor,
            workhrs:'2',
            percentage:20
        },

        {
            id: '3',
            title: 'Bedtime',
            image: imagepath.book,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            workhrs:'5',
            bgcolor: globalColor.activeworkcolor,
            percentage:40,
        },

    ])


    return (
        <View>
            <FlatList
                data={work}
                keyExtractor={(item, index) => index.toString()}
                style={styles.flastlist}
                renderItem={({ item }) => (
                    <View style={styles.workView}>
                        <View key={item.id} style={globlaStyle.row}>
                            <View style={[{ backgroundColor: item.bgcolor }, styles.imgebg]}>
                                <View style={styles.itemimgeview}>
                                    <Image
                                        source={item.image}
                                        style={styles.img2}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={styles.worktextview}>
                                <Text style={styles.worktext}>{item.title}</Text>
                            </View>
                            <View style={styles.loaderView}>
                                <LineLoader backgroundColor={globlaStyle.bgcolors} percentage={item.percentage} />
                            </View>
                            <View style={styles.workhrsview}>
                                <View style={globlaStyle.row}>
                                    <Text style={styles.hrs}>{item.workhrs}</Text>
                                    <Text style={styles.htext}>{'h'}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>

    )
};


export default LegList;

const styles = StyleSheet.create({
    workView:{
        paddingVertical:hp('1%')
    },
    worktextview:{
        justifyContent:'center',
        flex:0.35,
    },
    workimage:{
        flex:0.3,
      
    },
    viewprogressbar:{
        alignItems:'center',
        paddingTop:hp('1.5%')
    },
    loaderView:{
        justifyContent:'center',
        flex:0.38,
        paddingTop:hp('.5%')
    },
    hrs:{
        fontSize:hp('3%'),
        fontWeight:'bold',
        marginLeft:hp('3%')
    },
    workhrsview:{
        flex:0.3,
        justifyContent:'center',
        //alignItems:'center'
        
    },
    htext:{
        fontSize:hp('1.8%'),
        fontWeight:'bold'
    },
    itemimgeview:{
        height:hp('8%'),
        width:hp('8%'),
        alignItems:'center',
        justifyContent:'center'
    },
    img2: {
        width: '50%',
        height: '50%',
        borderRadius:hp('2%')
    },
    worktext:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        left:hp('2%')
    },
});