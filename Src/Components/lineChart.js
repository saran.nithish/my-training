import React from 'react';
import {Text,Dimensions,} from 'react-native';
import {LineChart,} from 'react-native-chart-kit';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const MyBezierLineChart = () => {

  const Gradient = () => (
    <Defs key={'gradient'}>
      <LinearGradient id={'gradient'} x1={'0'} y={'0%'} x2={'100%'} y2={'0%'}>
        <Stop offset={'0%'} stopColor={'rgb(134, 65, 244)'}/>
        <Stop offset={'100%'} stopColor={'rgb(66, 194, 244)'}/>
      </LinearGradient>
    </Defs>
  )

  return (
    <>
     
      <LineChart
        data={{
          labels: ['January', 'February', 'March', 'April'],
          datasets: [
            {
              data: [
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
              ],
            },
          ],
        }}
        width={Dimensions.get('window').width - 20}
        height={250}
       // yAxisLabel={'Rs'}
        chartConfig={{
          
          backgroundGradientFrom:'#F5FFFA',
          backgroundGradientTo: '#F5FFFA',
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => 'rgb(152,162,240)',
          labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
          style: {
            borderRadius: 16,
          },
        }}
        bezier
        contentInset={ { top: 20, bottom: 20 } }
        withInnerLines={false}
      withOuterLines={false}
        // svg={{
        //   strokeWidth: 2,
        //   stroke: 'url(#gradient)',
        // }}
        style={{
          marginVertical:hp('2%'),
          borderRadius:hp('2%'),
        }}
        
      >
    
       <Gradient/>
       </LineChart>
    </>
  );
};

export default  MyBezierLineChart;