import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity,LogBox  } from 'react-native';
import AnimatedCircularProgress from 'react-native-conical-gradient-progress';
import globalColor from '../globalstyles/color';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Entypo from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { fill: 20 };
    }
    
    // componentWillReceiveProps(){
    //     LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
    // }
     

    render() {

        return (


            <AnimatedCircularProgress
                size={250}
                width={20}
                fill={this.state.fill}
                prefill={100}
                beginColor={globalColor.bgcolors}
                endColor={globalColor.bgcolors2}
                segments={10}
                backgroundColor="rgba(255, 255, 255, 0.2)"
                lineCap="round"
                useAnimatedDriver={true}
            >
                {fill => (
                    <View style={style.titleBox}>
                        <View style={style.workviewspace}>
                            <Text style={style.title}>
                                Work
                </Text>
                        </View>
                        <Text style={style.time}>
                            4:37 
                            {/* {fill.toFixed(0)}%` */}
                </Text>
                        <TouchableOpacity onPress={() => this.setState({ fill: 100 })} style={style.viewspace}>
                            <LinearGradient colors={[globalColor.bgcolors, globalColor.bgcolors2]} style={style.linerview} start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 0 }}>
                                <Entypo
                                    name="controller-paus"
                                    size={hp('3%')}
                                    color={globalColor.white}
                                />
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                )}

            </AnimatedCircularProgress>

            //  <AnimatedCircularProgress
            //     size={107}
            //     width={10}
            //     fill={this.state.fill}
            //     segments={16}
            //     prefill={100}
            //     beginColor="#A9E5BE"
            //     endColor="#ffffff"
            //     backgroundColor="rgba(255, 255, 255, 0.2)"
            //     linecap="round"
            //   >
            //     {fill => (
            //       <View style={style.titleBox}>
            //         <Text style={style.title}>
            //           16 segments{'\n'}
            //           {fill.toFixed(0)}%
            //         </Text>
            //       </View>
            //     )}
            //   </AnimatedCircularProgress>

        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4FC87A',
    },
    titleBox: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
        bottom: hp('2%')
    },
    title: {
        fontSize: hp('2%'),
        color: globalColor.black,
        textAlign: 'center',
        fontWeight:'bold'
    },

    time: {
        fontSize: hp('5%'),
        color: globalColor.black,
        fontWeight: 'bold'
    },
    linerview: {
        height: hp('6%'),
        width: hp('6%'),
        borderRadius: hp('2%'),
        justifyContent: 'center',
        alignItems: 'center',

    },
    viewspace: {
        paddingTop:hp('3%')
    },
    workviewspace: {
        paddingTop:hp('2%')
    }
});