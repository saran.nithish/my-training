import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../globalstyles/styles';
import globalColor from '../globalstyles/color';
/////library
import Icon from 'react-native-vector-icons/Ionicons';
import { imagepath } from '../globalstyles/ImagePath';
import { fontProperties } from '../globalstyles/font';


const LegList = (props) => {
    const [skills, setskills] = useState([
        {
            id: '1',
            title: 'Arms',
            image: imagepath.workout1,
            des: 'Bread,Peanut butter,Apple',
            bgcolor: globalColor.item1,
            ka: '521'
        },
        {
            id: '2',
            title: 'Abs',
            image: imagepath.workout2,
            des: 'Salmon,Mixed veggles',
            bgcolor: globalColor.item2,
            ka: '602'
        },

        {
            id: '3',
            title: 'Legs',
            image: imagepath.workout3,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.snack
        },
        {
            id: '3',
            title: 'Glutes',
            image: imagepath.workout1,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.snack
        },
    ])


    return (
        <View>
            <FlatList
                data={skills}
                keyExtractor={(item, index) => index.toString()}
                style={styles.flastlist}
                numColumns={2}
                renderItem={({ item }) => (
                    <View style={styles.card}>
                        <View style={styles.subcard}>
                            <View style={styles.imagespace}>
                                <Image
                                    source={item.image}
                                    style={styles.itemimg}
                                />
                            </View>
                            <Text style={styles.itemtext}>{item.title}</Text>
                        </View>
                    </View>
                )}
            />
        </View>

    )
};


export default LegList;

const styles = StyleSheet.create({
    card: {
        width: '50%',
        height:'60%',
       paddingVertical: hp('1.5%'),
        paddingHorizontal: hp('1.5%'),
      },
      subcard: {
        backgroundColor:globalColor.white,
        borderRadius: hp('1.5%'),
        paddingVertical:hp('2%'),
        elevation: 3,
        
      },
   itemimg:{
     width: hp('12%'), 
    height: hp('12%'), 
    borderRadius: hp('12%'), 
    alignSelf: 'center'

    },
    itemtext:{
      textAlign:'center',
      //bottom:4,
      top:hp('.5%'),
      fontFamily:fontProperties.regular,
      fontSize:hp('1.8%'),
      color:globalColor.blueicon

    },
    areaText:{
        fontSize:hp('2%'),
        fontFamily:fontProperties.semiBold,
        color:globalColor.black
    },
    flastlist:{
       // paddingVertical:hp('2%'),
       bottom:hp('1%')
       
    },
    imagespace:{
        paddingVertical:hp('2%')
    }
});