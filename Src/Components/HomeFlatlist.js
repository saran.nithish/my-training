import React,{useState} from 'react';
import { View, Text, Image,TouchableOpacity,StyleSheet } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../globalstyles/styles';
import globalColor from '../globalstyles/color';
/////library
import { imagepath } from '../globalstyles/ImagePath';
import LinearGradient from 'react-native-linear-gradient';
import { fontProperties } from '../globalstyles/font';


const HomeFlatlist = (props) => {
    const [skills, setskills] = useState([
        {
            id: '1',
            title: 'BreakFast',
            image: imagepath.food1,
            des: 'Bread,Peanut butter,Apple',
            bgcolor: globalColor.item1,
            bgcolor2: globalColor.item12,
            ka: '521'
        },
        {
            id: '2',
            title: 'Lunch',
            image: imagepath.food2,
            des: 'Salmon,Mixed veggles',
            bgcolor: globalColor.item2,
            bgcolor2: globalColor.item22,
            ka: '602'
        },

        {
            id: '3',
            title: 'Snack',
            image: imagepath.food2,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.item3,
            bgcolor2: globalColor.item32,
        },
    ])


    return (



        <View style={globlaStyle.row}>
            {skills.map((item, e) => {
                return (
                    <View key={item.id} style={styles.mealboxview}>
                        <TouchableOpacity onPress={() => { props.navigation.navigate('Training') }}>
                            <LinearGradient colors={[item.bgcolor, item.bgcolor2]} style={[styles.mealbox]}>
                                    <View style={styles.imgview}>
                                        <Image
                                            source={item.image}
                                            style={styles.img}
                                            resizeMode='stretch'
                                        />
                                    </View>
                                    <View style={styles.titleview}>
                                            <Text style={styles.itemtitle}>{item.title}</Text>
                                            <Text style={styles.itemdes}>{item.des}</Text>
                                        <View style={[globlaStyle.row, styles.desView]}>
                                                <Text style={styles.itemka}>{item.ka}</Text>
                                                <Text style={styles.kal}> Kcal</Text>
                                        </View>
                                    </View>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                )
            })
            }
        </View>

    )
};


export default HomeFlatlist;

const styles = StyleSheet.create({
    mealboxview: {
        paddingHorizontal: hp('1%'),
        paddingVertical: hp('2.5%')
    },
    imgview: {
        bottom: hp('4%'),
        width: '50%',
        height: '38%',
        marginLeft:hp('1%')
    },
    img: {
        width: '100%',
        height: '100%'
    },
    titleview:{
        paddingHorizontal:hp('2%'),
         bottom: hp('4%')
    },
    desView:{
        paddingTop:hp('5%')
    },
    itemtitle:{
        color:globalColor.white,
        fontSize:hp('2%'),
        fontFamily:fontProperties.semiBold
    },
    itemdes:{
        color:globalColor.white,
        fontSize:hp('1.5%'),
        fontFamily:fontProperties.regular
    },
    itemka:{
        color:globalColor.white,
        fontSize:hp('3%'),
        fontFamily:fontProperties.regular,
        letterSpacing:2
    },
    kal:{
        color:globalColor.white,
        fontSize:hp('1.5%'),
        fontFamily:fontProperties.regular,
        top:hp('1.5%')
    },
    tie:{
        marginLeft:hp('.2%'),
        //paddingTop:hp('1%')
    },
    dumb: {
        backgroundColor: globalColor.boxcolor,
       // paddingVertical: hp('.8%'),
        width: hp('10%'),
        height:hp('10%'),
        alignItems: 'center', 
        borderBottomRightRadius:hp('10%')
    },
    mealbox: {
        width: hp('15%'),
        height: hp('25%'),
        //backgroundColor: '#fff',
        borderTopRightRadius:hp('8%'),
        borderTopLeftRadius:hp('1%'),
        borderBottomLeftRadius:hp('1%'),
        borderBottomRightRadius:hp('1%'),
        // paddingVertical:hp('3%')
        // paddingHorizontal:hp('5%')
        elevation: 3,

    },
});