import React from 'react';
import {
  View,
  LogBox
 
} from 'react-native';
 
import ProgressBarAnimated from 'react-native-progress-bar-animated';
 
export default class App extends React.Component {
 
  state = {
    progress:this.props.percentage,
    progressWithOnComplete: 0,
    progressCustomized: 0,
    color:this.props.backgroundColor
  }

//   componentWillReceiveProps(){
//   LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
// }

 
  increase = (key, value) => {
    this.setState({
      [key]: this.state[key] + value,
    });
  }
 
  render() {
   
   ///console.log(this.state.color)
    return (
        <View>
          <ProgressBarAnimated
            width={100}
            height={10}
            value={this.state.progress}
            //backgroundColorOnComplete={this.state.color}
            backgroundColor={this.state.color}
            borderColor={'gray'}
          />

        </View>
    );
  }
}
 
