import React,{useState} from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet,FlatList } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../globalstyles/styles';
import globalColor from '../globalstyles/color';
/////library
import Icon from 'react-native-vector-icons/Ionicons';
import { imagepath } from '../globalstyles/ImagePath';
import { fontProperties } from '../globalstyles/font';


const LegList = (props) => {
    const [skills, setskills] = useState([
        {
            id: '1',
            title: 'Squet and Walk',
            image: imagepath.workout1,
            des: 'Bread,Peanut butter,Apple',
            bgcolor: globalColor.item1,
            sec: '45 seconds'
        },
        {
            id: '2',
            title: 'Pile Squat and heel Raises',
            image: imagepath.workout6,
            des: 'Salmon,Mixed veggles',
            bgcolor: globalColor.item2,
            sec: '45 seconds'
        },

        {
            id: '3',
            title: 'Squat Kickback',
            image: imagepath.workout4,
            des: 'Recommend:800 Kcal',
            sec: '45 seconds',
            bgcolor: globalColor.snack
        },
        {
            id: '4',
            title: 'Squat with Side Leg Lift',
            image: imagepath.workout1,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.snack,
            sec: '45 seconds'
        },
        {
            id: '5',
            title: 'Squat with Side Leg Lift',
            image: imagepath.workout4,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.snack,
            sec: '45 seconds'
        },
        {
            id: '6',
            title: 'Squat with Side Leg Lift',
            image: imagepath.workout6,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.snack,
            sec: '45 seconds'
        },
    ])


    return (


        <View>
            <FlatList
                data={skills}
                keyExtractor={(item, index) => index.toString()}
                style={styles.flastlist}
                renderItem={({ item }) => (
                    <View style={[globlaStyle.row, styles.Bottomlinespace]}>
                        <View style={styles.imgeFlex}>
                            <View style={styles.imgView}>
                                <Image
                                    source={item.image}
                                    style={styles.image}
                                    resizeMode='contain'
                                />
                                <View style={styles.circleShap2}>
                                    <TouchableOpacity>
                                        <Icon name={'play'} color={globalColor.black} size={15} style={styles.playIconStyle} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                                <View style={globlaStyle.row}>
                                    <View style={styles.linespace}>
                                        <Text style={styles.linecolor}>{'--'}</Text>
                                    </View>
                                    <View style={styles.restsec}>
                                        <Text style={styles.sectext} >15s rest</Text>
                                    </View>
                                    <View style={styles.linespace}>
                                        <Text style={styles.linecolor}>{'------------------'}</Text>
                                    </View>
                                </View>
                        </View>
                        <View style={[globlaStyle.flex, styles.justifyContent]}>
                                    <Text style={styles.itemtitle}>{item.title}</Text>
                                    <Text style={styles.itemsec}>{item.sec}</Text>
                            <View style={styles.linespace2}>
                                <Text style={styles.linecolor}>{'----------------------------------------------------'}</Text>
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>

    )
};


export default LegList;

const styles = StyleSheet.create({
    imgeFlex:{
        flex:0.6,
        alignItems:'flex-start',
        right:hp('1%')
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius:hp('2%')
    },
    itemtitle: {
        fontFamily: fontProperties.semiBold,
        fontSize: hp('2%'),
        color: globalColor.titleitem
    },
    itemsec: {
        fontFamily: fontProperties.regular,
        fontSize: hp('1.8%'),
        color: globalColor.lightgray
    },
    flastlist:{
        paddingVertical:hp('1%')
    },
   Bottomlinespace:{
        paddingVertical:hp('2%'),
    },
    circleShap2: {
        elevation: 4,
        position: "absolute",
        zIndex: 100,
        right: hp('5%'),
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp('3%'),
        height: hp('4%'),
        width: hp('4%'),
        borderRadius: hp('4%'),
        backgroundColor: "#fff"
      },
    playIconStyle:{
        alignSelf: 'center',
        left:hp('.2%')
    },
    restsec:{
        backgroundColor: globalColor.secbox, 
        borderRadius: hp('1%'), 
        paddingHorizontal: hp('1%'), 
        top: hp('2.2%')
    },
    linespace:{
        top:hp('2%')
    },
    linespace2:{
       top:hp('3.8%')
    },
    sectext:{
        fontFamily:fontProperties.regular,
        fontSize:hp('1.5%'),
        color:globalColor.blueicon
    },
    linecolor:{
        color:globalColor.lightgray
    },
    burnIcon:{
        paddingTop:hp('.5%')
    },
    justifyContent: {
        justifyContent: 'center'
    },
    imgView: {
        height: hp('10%'),
        width: hp('15%'),
    },
});