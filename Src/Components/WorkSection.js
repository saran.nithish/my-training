import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../globalstyles/styles';
import globalColor from '../globalstyles/color';
/////library
import { imagepath } from '../globalstyles/ImagePath';


const WorkSection = (props) => {
    const [skills, setskills] = useState([
        {
            id: '1',
            title: 'Work',
            image: imagepath.work,
            bgcolor: globalColor.white,
            ka: '521'
        },
        {
            id: '2',
            title: 'Sport',
            image: imagepath.football,
            des: 'Salmon,Mixed veggles',
            bgcolor: globalColor.tranperentcolor,
            ka: '602'
        },

        {
            id: '3',
            title: 'Bedtime',
            image: imagepath.book,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.tranperentcolor,
        },
        {
            id: '4',
            title: 'Study',
            image: imagepath.sleep,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            bgcolor: globalColor.tranperentcolor,
        },
    ])



    return (

        <View style={globlaStyle.row}>
            {skills.map((item, e) => {
                return (
                    <View key={item.id} style={styles.mealboxview}>
                        <TouchableOpacity onPress={() => { props.navigation.navigate('youramis') }}>
                            <View style={[{ backgroundColor: item.bgcolor }, styles.imgebg]}>
                                <View style={styles.itemimgeview}>
                                    <Image
                                        source={item.image}
                                        style={styles.img2}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={styles.textview}>
                                <Text style={styles.activeText}>{item.title}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            })
            }
        </View>

    )
};


export default WorkSection;

const styles = StyleSheet.create({
    mealboxview: {
        paddingHorizontal: hp('2%'),
        paddingVertical: hp('2.8%')
    },
    imgebg: {
        borderRadius: hp('3%')
    },
    itemimgeview: {
        height: hp('8%'),
        width: hp('8%'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    img2: {
        width: '50%',
        height: '50%',
        borderRadius: hp('2%')

    },
    activeText: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: globalColor.white
    },
    itemimgeview: {
        height: hp('8%'),
        width: hp('8%'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    textview: {
        paddingVertical: hp('1%')
    },
});