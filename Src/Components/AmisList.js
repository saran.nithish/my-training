import React, { useState } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
///styles
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import globlaStyle from '../globalstyles/styles';
import globalColor from '../globalstyles/color';
/////library
import { imagepath } from '../globalstyles/ImagePath';
import LinearGradient from 'react-native-linear-gradient';
import LineLoader from '../Components/lineLoader';


const AmisList = (props) => {
    const [work, setwork] = useState([
        {
            id: '1',
            title: 'Work',
            image: imagepath.laptop,
            bgcolor: globalColor.youramisitem1color,
            workhrs: '8',
            percentage: 50
        },
        {
            id: '2',
            title: 'Sport',
            image: imagepath.dumbbell,
            des: 'Salmon,Mixed veggles',
            bgcolor: globalColor.youramisitem2color,
            workhrs: '2',
            percentage: 50
        },

        {
            id: '3',
            title: 'Bedtime',
            image: imagepath.laptop,
            des: 'Recommend:800 Kcal',
            //ka: '521',
            workhrs: '5',
            bgcolor: globalColor.youramisitem3color,
            percentage: 40,
        },

    ])



    return (

        <View style={globlaStyle.row}>
            {work.map((item, e) => {
                return (
                    <View key={item.id} style={styles.boxview}>
                        <LinearGradient colors={[globalColor.white, item.bgcolor]}
                            style={styles.boxStyle}
                            start={{ x: 0, y: 2 }}
                            end={{ x: 0, y: 0 }}>
                            <View>
                                <View style={styles.imgview}>
                                    <Image
                                        source={item.image}
                                        style={styles.img}
                                        resizeMode='stretch'
                                    />
                                </View>
                                <View style={styles.itemtextView}>
                                    <View>
                                        <Text style={styles.title}>{item.title}</Text>
                                    </View>
                                    <View style={globlaStyle.row}>
                                        <Text style={styles.hrs}>{item.workhrs}</Text>
                                        <Text style={styles.htext}>{'h'}</Text>
                                    </View>
                                    <View style={styles.itemviewSpace}>
                                        <LineLoader backgroundColor={item.bgcolor} percentage={item.percentage} />
                                    </View>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                )
            })
            }
        </View>

    )
};


export default AmisList;

const styles = StyleSheet.create({
    boxview: {
        paddingVertical: hp('2%'),
        paddingHorizontal: hp('2%')
    },
    boxStyle: {
        width: wp('40%'),
        height: hp('25%'),
        borderRadius: hp('5%')
    },
    imgview: {
        bottom: hp('5%'),
        width: '80%',
        height: '70%',
        marginLeft:hp('1%')
    },
    img: {
        width: '100%',
        height: '100%'
    },
    hrs:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        color:globalColor.white
        //marginLeft:hp('3%')
    },
    htext:{
        fontSize:hp('1.5%'),
        fontWeight:'bold',
        color:globalColor.white
    },
    title:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        color:globalColor.white
    },
    itemtextView:{
        bottom: hp('5%'), 
        marginLeft: hp('2%')
    },
    itemviewSpace:{
        paddingVertical:hp('1.5%')
    },
    listview:{
        paddingVertical: hp('4%')
    },
});