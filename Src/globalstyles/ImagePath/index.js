export const imagepath = {
    addBtn: require('../../globalstyles/Image/addbtn.png'),
    food1: require('../../globalstyles/Image/food2.png'),
    food2: require('../../globalstyles/Image/food5.png'),
    food3: require('../../globalstyles/Image/food3.png'),
    playBtn: require('../../globalstyles/Image/play.png'),
    runimage:require('../../globalstyles/Image/run.png'),
    runimage2:require('../../globalstyles/Image/run2.png'),
    workout1:require('../../globalstyles/Image/workout1.png'),
    workout2:require('../../globalstyles/Image/workout2.jpg'),
    workout3:require('../../globalstyles/Image/workout3.jpg'),
    workout6:require('../../globalstyles/Image/workout6.jpg'),
    workout4:require('../../globalstyles/Image/workout4.jpg'),
    workout5:require('../../globalstyles/Image/workout5.jpg'),
    profile:require('../../globalstyles/Image/profile.jpg'),
    book:require('../../globalstyles/Image/book.jpg'),
    football:require('../../globalstyles/Image/football.jpg'),
    sleep:require('../../globalstyles/Image/sleep.jpg'),
    work:require('../../globalstyles/Image/work.png'),
    dumbbell:require('../../globalstyles/Image/dumbbell2.png'),
    laptop:require('../../globalstyles/Image/laptop.png'),
}


export default imagepath
