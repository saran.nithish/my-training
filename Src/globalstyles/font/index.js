export const fontProperties = {
    bold: 'Poppins-Bold',
    italic: 'Poppins-Italic',
    italicbold: 'Poppins-SemiBoldItalic',
    regular: 'Poppins-Light',
    semiBold: 'Poppins-SemiBold',
    extraBold: 'Poppins-ExtraBold',
    Medium:'Poppins-Medium'
  };
