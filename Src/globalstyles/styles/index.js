import { StyleSheet } from 'react-native';
///font & color
import globalColor from '../color'
import { fontProperties } from '../font';
///////library
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
 global:{
     width:'100%',
     height:'100%',
     backgroundColor:globalColor.backgoundcolor
    
 },
 row:{
     flexDirection:'row',
    
 },
 flex:{
     flex:1
 },

headerTitle:{
    fontSize:hp('3%'),
    fontFamily:fontProperties.semiBold
},
subtitle:{
    fontSize:hp('2%'),
    fontFamily:fontProperties.Medium
} ,
col:{
    flexDirection:'column'
},
profileimage: {
    width: hp('7%'),
    height: hp('7%'),
    borderRadius: hp('3%')
},



  
});
