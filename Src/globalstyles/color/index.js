export const colors = {
  ///primarycolor
  white: '#fff',
  black: '#000',
  // primaryColor: '#FFCD1C',
  activecolor: '#7F8BC7',
  inactivecolor: '#9192A2',
  backgoundcolors: '#F2F3F8',
  EastenColor: '#CFD8FE',
  burnedColor: '#FBD1DE',
  lightgray: '#9391A4',
  bottomcolor: '#E5EAFA',
  snack: '#F17DA2',
  fat: '#FCC44A',
  bnt1: '#F87981',
  bnt2: '#FDA290',

  ///flast
  item1: '#F98183',
  item12:'#FDA692',
  item2: '#7185E8',
  item22:'#5D63DF',
  item3:'#FD8FB1',
  item32:'#FB6794',
  titleitem: '#636173',
  ///chart
  chart: '#ddd',
  cri: '#5869DF',
  cri2: '#6B8DE7',
  boxcolor: '#8999DA',

  ///icon
  arrowicon: '#252541',
  blueicon: '#577DE5',
  secbox: '#EAEDFD',

  ////work/////
  bgcolors:'#4EC6FB',
  bgcolors2:'#E4ACF5',
  tranperentcolor:'#FFFAFA',
  activeworkcolor:'#F5FFFA',
  ///youramis
  youramisitem1color:'#F7C3F3',
  youramisitem2color:'#B29CF7',
  youramisitem3color:'#BEE4FE',

  bellIcon:'#DA7AFE',
  crossIcon:'#A8CCF6'

};

export default colors;
