import React, { useEffect } from 'react';
///navigationlibray
import { NavigationContainer, } from '@react-navigation/native';
import StackScreens from './Src/navigation/stack';



const App = () => {

  return (
    
      <NavigationContainer >
        <StackScreens />
      </NavigationContainer>

  );
}

export default App;
